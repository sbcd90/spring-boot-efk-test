FROM openjdk:8
COPY target/spring-boot-hello-world-1.0-SNAPSHOT.jar /tmp
WORKDIR /tmp

CMD java -jar /tmp/spring-boot-hello-world-1.0-SNAPSHOT.jar