package com.sap.i076326.kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.protocol.types.Field;

import java.time.Duration;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SimpleKafkaConsumer {

    private static final String KAFKA_TOPIC = "integration";

    private static final String KAFKA_BROKERS = "pkc-4r087.us-west2.gcp.confluent.cloud:9092";

    private static final String KAFKA_USERNAME = "NYDW2BS7R24APLZ2";

    private static final String KAFKA_SECRET = "oEjw0skINeWBm9g9x/wChygq+v4+vjFREu1m0uRRdgoB/kPMWwFB90TN600VFExq";

    private Map<String, Object> props;

    private KafkaConsumer<String, String> kafkaConsumer;

    public SimpleKafkaConsumer() {
        this.props = new HashMap<>();

        this.props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_BROKERS);
        this.props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringDeserializer");
        this.props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringDeserializer");

        String jaasTemplate = "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"%s\" password=\"%s\";";
        String jaasConfig = String.format(jaasTemplate, KAFKA_USERNAME, KAFKA_SECRET);

        this.props.put("security.protocol", "SASL_SSL");
        this.props.put("sasl.mechanism", "PLAIN");
        this.props.put("sasl.jaas.config", jaasConfig);

        this.props.put("group.id", KAFKA_TOPIC);
        this.props.put("enable.auto.commit", true);
        this.props.put("auto.commit.interval.ms", "1000");
        this.props.put("session.timeout.ms", "30000");

        this.kafkaConsumer = new KafkaConsumer<String, String>(props);
    }

    public void receiveRecords() {
        this.kafkaConsumer.subscribe(Arrays.asList(KAFKA_TOPIC));

        try {
            while (true) {
                ConsumerRecords<String, String> records = this.kafkaConsumer.poll(Duration.ofMillis(1000));

                Iterator<ConsumerRecord<String, String>> recordIterator = records.iterator();

                while (recordIterator.hasNext()) {
                    ConsumerRecord<String, String> consumerRecord = recordIterator.next();
                    System.out.println(consumerRecord.key() + " - " + consumerRecord.value());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SimpleKafkaConsumer kafkaConsumer = new SimpleKafkaConsumer();
        kafkaConsumer.receiveRecords();
    }
}