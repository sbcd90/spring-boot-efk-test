package com.sap.i076326.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class SimpleKafkaProducer {

    private static final String KAFKA_TOPIC = "integration";

    private static final String KAFKA_BROKERS = "pkc-4r087.us-west2.gcp.confluent.cloud:9092";

    private static final String KAFKA_USERNAME = "NYDW2BS7R24APLZ2";

    private static final String KAFKA_SECRET = "oEjw0skINeWBm9g9x/wChygq+v4+vjFREu1m0uRRdgoB/kPMWwFB90TN600VFExq";

    private Map<String, Object> props;

    private KafkaProducer<String, String> kafkaProducer;

    public SimpleKafkaProducer() {
        this.props = new HashMap<>();

        this.props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_BROKERS);
        this.props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringSerializer");
        this.props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringSerializer");

        String jaasTemplate = "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"%s\" password=\"%s\";";
        String jaasConfig = String.format(jaasTemplate, KAFKA_USERNAME, KAFKA_SECRET);

        this.props.put("security.protocol", "SASL_SSL");
        this.props.put("sasl.mechanism", "PLAIN");
        this.props.put("sasl.jaas.config", jaasConfig);

        this.kafkaProducer = new KafkaProducer<String, String>(this.props);
    }

    public void sendRecords(String key, String value) {

        for (int count = 0; count < 10; count++) {
            int partition = new Random().nextInt(12);
            ProducerRecord<String, String> producerRecord = new ProducerRecord<>(KAFKA_TOPIC, partition,
                    key + partition, value + partition);
            this.kafkaProducer.send(producerRecord);
        }
        this.kafkaProducer.close();
    }

    public static void main(String[] args) {
        SimpleKafkaProducer kafkaProducer = new SimpleKafkaProducer();
        kafkaProducer.sendRecords("hello", "world");
    }
}